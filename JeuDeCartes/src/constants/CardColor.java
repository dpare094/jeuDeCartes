package constants;

/**
 * 
 * @author DAP
 *
 */
public enum CardColor {
	CARREAU("Carreau"), COEUR("Coeur"), TREFLE("Tr�fle"), PIQUE("Pique");

	/*
	 * 
	 */
	private String name = "";

	private CardColor(String name) {
		this.name = name;
	}

	public String toString() {
		return name;
	}

}
