package entities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import constants.CardColor;
import constants.CardFigure;

/**
 * Un paquet de carte est compos�
 * - d'un deck de 52 cartes 
 * 
 * @author DAP
 *
 */
public class Deck {

	private List<Card> deck = null;

	/**
	 * 
	 */
	public Deck() {
		
		this.deck = new ArrayList();
		/*
		 * Remplissage du deck cr��
		 */
		this.createDeck();
	}

	/***
	 *  
	 */
	public List<Card> getDeck() {
		return deck;
	}

	public void add(Card card) {
		this.deck.add(card);
	}
	

	@Override
	public String toString() {
		return "Deck [deck=" + deck + "]";
	}

	
	/**
	 * createDeck
	 * Cr�ation du deck
	 * 
	 */
	public void createDeck() {
		/*
		 * Pour chaque couleur
		 */
		for (CardColor color : CardColor.values()) {

			/*
			 *  Ajout des cartes de 2 � 10
			 */
			for (int i = 2; i < 11; i++)
				this.deck.add(new Card(Integer.toString(i), color.toString()));
			
			/*
			 * Ajout des autres cartes
			 */
			this.deck.add(new Card(CardFigure.VALET, color.toString()));
			this.deck.add(new Card(CardFigure.DAME, color.toString()));
			this.deck.add(new Card(CardFigure.ROI, color.toString()));
			this.deck.add(new Card(CardFigure.AS, color.toString()));

		}
	}

	/**
	 * shuffleDeck
	 * Melange du deck
	 * 
	 */
	public void shuffleDeck() {
		Collections.shuffle(this.deck);
	}

}
