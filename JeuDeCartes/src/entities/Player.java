package entities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Un joueur compos�  
 * - d'un num�ro de joueur
 * - d'une main, une liste de carte
 * - de plis, une liste de carte gagn� lors d'un tour
 * 
 * @author DAP
 *
 */
public class Player {

	private int playerNumber;
	private List<Card> hand;
	private List<Card> plis;
	
	/**
	 * 
	 */
	public Player() {
	}
	
	public Player(int playerNumber) {
		this.playerNumber = playerNumber;
		this.hand = new ArrayList<Card>();
		this.plis = new ArrayList<Card>();
	}
	

	/**
	 * 
	 */
	public int getPlayerNumber() {
		return playerNumber;
	}
	public void setPlayerNumber(int playerNumber) {
		this.playerNumber = playerNumber;
	}	
	
	public List<Card> getHand() {
		return hand;
	}
	public List<Card> getPlis() {
		return plis;
	}

	public void addCardToHand(Card card) {
		this.hand.add(card);
	}
	public void addCardToPlis(Card card) {
		this.plis.add(card);
	}
	


	@Override
	public String toString() {
		return "Player "+playerNumber +":[hand=" + hand + "]";
	}	
	
	/**
	 * showPli 
	 * 		affiche :
	 * 			le numero du joueur
	 * 			le nombre de pli gagn�
	 * 			la puissance des cartes du pli			
	 * 			le contenu du pli
	 * 
	 * @return
	 */
	public String showPli (int nbPlayer) {
	
		int power = 0;
		String pli; 
		
		for (int i =0 ; i< this.getPlis().size() ; i++)
			power += this.getPlis().get(i).cardPower();
		pli = "Player "+playerNumber+": "+plis.size() /nbPlayer +" / "+power+"\nPlis : "+ plis;

		return pli;
		
	}
	
}
