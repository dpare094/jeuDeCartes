package entities;

import constants.CardFigure;

/**
 * Une carte est compos�e 
 *  - d'une valeur 
 *  - d'une couleur
 * 
 * @author DAP
 *
 */
public class Card {

	private String value;	
	private String color;

	/**
	 * 
	 */
	public Card() {}
	
	public Card(String value, String color) {	
		this.value = value;
		this.color = color;
	}

	/***
	 *  
	 */
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}

	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}

	/**
	 * 
	 */
	@Override
	public String toString() {
		return value + " de " + color;
	}

	/**
	 * getCardPower
	 * On d�finie une puissance pour la carte en fonction de sa valeur
	 * 
	 * @return
	 * 		puissance de la carte actuelle
	 */
	public int cardPower() {
		
		int power;
		/*
		 * plus la carte est forte plus sa puissance est haute
		 */
		switch (this.value) {
			case CardFigure.AS: 	power = 14; break;
			case CardFigure.ROI:	power = 13;break;
			case CardFigure.DAME:	power = 12;break;
			case CardFigure.VALET:	power = 11;break;
			default:				power = Integer.parseInt(this.value);
		}
		
		return power;
	}
	
	/**
	 * compareCards
	 * Test de l'�galit� de la carte actuelle avec une autre carte
	 * 
	 * @param card
	 * @return
	 *  	<0 	si la carte param�tre est sup�rieure
	 *  	= 0 si les deux cartes sont identiques
	 *  	>0 	si la carte testant est sup�rieure
	 */
	public int compareCards(Card card) {
		
		return this.cardPower() - card.cardPower();
	}


}
