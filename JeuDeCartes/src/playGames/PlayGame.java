package playGames;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import entities.Deck;
import entities.Player;

/**
 * 
 * @author DAP
 * [Class contenant un Main]
 *
 */
public class PlayGame {

	Deck myDeck;
	List <Player>lesPlayers;
 
	/**
	 * 
	 */
	public PlayGame() {
		myDeck = new Deck();	
	}

	/**
	 * divideDeck
	 * Partage des cartes du deck, une par une entre les joueurs
	 * 
	 * @param players
	 */
	public void divideDeck(List<Player> players) {
		// il faut que chaque joueur ai le m�me nombre de carte
		// les carte en trop sont ignor�es
		int nbCard = this.myDeck.getDeck().size() - (this.myDeck.getDeck().size() %  this.lesPlayers.size()); 
		
		// on reparti les cartes 
		for (int i = 0; i < (nbCard); i += this.lesPlayers.size()) {
			for (int j = 0; j< this.lesPlayers.size() ; j++)
				players.get(j).addCardToHand(this.myDeck.getDeck().get(i+j));
		}
	}
	

	/**
	 * GameTurn
	 * 
	 * @param turn
	 * @param players
	 */
	public void gameTurn (int turn, List<Player> players) {
		
		Player winner;
		
		/*
		 * Les joueurs sont plac�s dans une liste 
		 * afin de les m�langer pour traiter le cas des �galit�s de carte
		 */
		Collections.shuffle(players);
		
		/*
		 * Test de la carte de chaque joueur
		 * Le premier joueur est le premier de la liste
		 */
		winner = players.get(0);
		
		/*
		 * Boucle sur les autres joueurs
		 * le gagnant est celui avec la plus forte valeur
		 * en cas d'�galit�, c'est le premier de la liste 
		 * (qui ne sera pas le m�me, la liste des joueurs �tant m�lang� � chaque tour)
		 */
		for (int i= 1; i<players.size(); i++) {
			winner = winner.getHand().get(turn).compareCards( players.get(i).getHand().get(turn))< 0 ? players.get(i) : winner ;	
		}
		
		// Le gagnant prend toutes les cartes jou�es
		for (int i= 0; i<players.size(); i++) 
				winner.addCardToPlis(players.get(i).getHand().get(turn));	
	}

	
	
	/**
	 * Main
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		// Le nombre de joueur de 1 � x
		int nbPlayer = 4;
		
		/* 
		 * Cr�ation et melange du jeu
		 */
		PlayGame gameOne = new PlayGame();		
		gameOne.myDeck.shuffleDeck();	

		/*
		 * On place les joueurs dans une list
		 */
		gameOne.lesPlayers = new ArrayList<Player>();
		// on ajoute 4 joueurs
		for (int i = 0; i <nbPlayer ; i++)
			gameOne.lesPlayers.add(new Player(i+1));

		/*
		 * Distribution des cartes
		 */
		gameOne.divideDeck(gameOne.lesPlayers);
	
		for (int i = 0; i<nbPlayer; i++)
			System.out.println( gameOne.lesPlayers.get(i));
		
		/*
		 * Tours de jeu
		 */
		for (int i=0; i< (gameOne.myDeck.getDeck().size())/nbPlayer; i++) 			
			gameOne.gameTurn (i, gameOne.lesPlayers); 
			
		/*
		 *  Affichage des resultats 
		 */
		System.out.println("\nJoueur  :  nbre plis gagn�/ puissance des cartes"); 			
		// On retri la liste des joueurs dans l'ordre du numero joueur (la liste ayant �t� m�lang�e dans les tours de jeu), pour un affichage tri�e
		Collections.sort(gameOne.lesPlayers, new Comparator<Player>() {
		    @Override
		    public int compare(Player p1, Player p2) {
		        return p1.getPlayerNumber()-p2.getPlayerNumber();
		    }
		});
	
		// affichage du r�sultat
		for (int i=0; i< nbPlayer; i++) {			
			System.out.println(((Player)gameOne.lesPlayers.get(i)).showPli(nbPlayer));
		}
		
	}

}
