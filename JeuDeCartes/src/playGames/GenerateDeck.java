package playGames;

import entities.Card;
import entities.Deck;

/**
 * 
 * @author DAP
 * [Class contenant un Main]
 *
 *	Génération du deck et affichage simple pour test
 *
 */
public class GenerateDeck {

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		/*
		 * Initialisations
		 */
		Deck myDeck = new Deck();	
		
		/*
		 * Affichage du resultat
		 */
		for (Card card : myDeck.getDeck())
			System.out.println(card);
	}

}
